from xmlrunner.extra.djangotestrunner import XMLTestRunner


class JUnitTestRunner(XMLTestRunner):
    """
    Class with hack to prevent XMLTestRunner from using suffix for test suite names.

    The XMLTestRunner defaults to a timestamp as suffix preventing CI integrations
    to detect existing, fixed and new test cases.

    TODO: Remove when xmlrunner fixes this limitation.
    See: https://github.com/xmlrunner/unittest-xml-reporting/blob/master/xmlrunner/runner.py#L29
    """
    def get_test_runner_kwargs(self):
        """Override setting removing `outsuffix`."""
        runner_kwargs = super(JUnitTestRunner, self).get_test_runner_kwargs()
        runner_kwargs['outsuffix'] = ''
        return runner_kwargs

